from sr.robot import *
from time import sleep


#Net Class

class Net(object): #Class with functions that stores the colour of each face in a net, and is able to rotate the net, find the face a given colour is on and find a solution to get the team colour on top.
    
    def __init__(self, top, bottom, front, back, left, right): #Initalises face variables
        self.top = top
        self.bottom = bottom
        self.front = front
        self.back = back
        self.left = left
        self.right = right
    
    def rotate(self, direction): #Given a rotation direction it maps the rotation and updates the variables accordingly
        
        if direction == "back":
            tempFace = self.front
            self.front = self.back
            self.back = tempFace
            tempFace = self.left
            self.left = self.right
            self.right = tempFace
        
        elif direction == "left":
            tempFront = self.front
            tempBack = self.back
            tempLeft = self.left
            tempRight = self.right
            self.front = tempLeft
            self.left = tempBack
            self.back = tempRight
            self.right = tempFront
                
        elif direction == "right":
            tempFront = self.front
            tempBack = self.back
            tempLeft = self.left
            tempRight = self.right
            self.front = tempRight
            self.right = tempBack
            self.back = tempLeft
            self.left = tempFront
            
        elif direction == "forward":
            tempTop = self.top
            tempBottom = self.bottom
            tempFront = self.front
            tempBack = self.back
            self.front = tempBottom
            self.botom = tempBack
            self.back = tempTop
            self.top = tempFront
            
        elif direction == "left spin":
            tempTop = self.top
            tempBottom = self.bottom
            tempLeft = self.left
            tempRight = self.right
            self.top = tempRight
            self.left = tempTop
            self.bottom = tempLeft
            self.right = tempBottom
            
        elif direction == "right spin":
            tempTop = self.top
            tempBottom = self.bottom
            tempLeft = self.left
            tempRight = self.right
            self.top = tempLeft
            self.right = tempTop
            self.bottom = tempRight
            self.left = tempBottom
            
    def getFace(self, face): #Given a colour it finds which face in a net that colour is on
        
        if self.top == face:
            return "top"
        
        elif self.bottom == face:
            return "bottom"
            
        elif self.front == face:
            return "front"
        
        elif self.back == face:
            return "back"
        
        elif self.left == face:
            return "left"
        
        elif self.right == face:
            return "right"
            
    def adjustNet(self, faceColour, faceOrient): #Adjusts the net so that the net is in the correct orientation to match the side the robot is currently facing, and the orient of the token
        facePos = self.getFace(faceColour)
        if facePos == "top":
            self.rotate("forward")
            self.rotate("forward")
            self.rotate("forward")
        
        elif facePos == "back":
            self.rotate("forward")
            self.rotate("forward")
        
        elif facePos == "bottom":
            self.rotate("forward")
        
        elif facePos == "left":
            self.rotate("left")
        
        elif facePos == "right":
            self.rotate("right")
        
        if faceOrient == "left":
            self.rotate("right spin")
        
        elif faceOrient == "right":
            self.rotate("left spin")
        
        elif faceOrient == "bottom":
            self.rotate("left spin")
            self.rotate("left spin")
            
    def getSolution(self, targetColour): #Gets a solution to get the target colour on top. It returns a list of string commands that when executed in sequential order will go from the net's current state to having the target colour on top
        solution = []
        targetPos = self.getFace(targetColour)
        
        if targetPos == "front":
            solution.append("flip")
        
        elif targetPos == "bottom":
            solution.append("flip")
            solution.append("flip")
        
        elif targetPos == "back":
            solution.append("back")
            solution.append("flip") #Might be more efficent to triple flip here, need to test
        
        elif targetPos == "left":
            solution.append("left")
            solution.append("flip")
            
        elif targetPos == "right":
            solution.append("right")
            solution.append("flip")
            
        return solution
        
        

#Intialses robot component releated variables and objects
R = Robot()
lwheel = R.motors[0].m0
rwheel = R.motors[0].m1

#Changable global constants

accelerationStep = 5 #The increments of power to increase/decrease the motor power by
accelerationTime = 0.1 #The time between each acceleration step

forwardCallibration = 1 #Callibration to make one second of sleep equal to 1 metre of forward movement
quickForwardCallibration = 1 #Callibration to make one second of sleep equal to 1 metre of quick forward movement
reverseCallibration = 1 #Callibration to make one second of sleep equal to 1 metre of reverse movement
clockwiseCallibration = 1 #Calibration to make one second of sleep equal to 45 degrees of clockwise rotation
anticlockwiseCallibration = 1 #Calibration to make one second of sleep equal to 45 degrees of anticlockwise rotation

forwardAccelerationOffset = 1 #The distance in metres travelled during the acceleration and decelleration time forward
quickForwardAccelerationOffset = 1 #The distance in metres travelled during the acceleration and decelleration time quick forward
reverseAccelerationOffset = 1 #The distance in metres travelled during the acceleration and decelleration time in reverse
clockwiseAccelerationOffset = 1 #The distance in metres travelled during the acceleration and decelleration time turning clockwise
anticlockwiseAccelerationOffset = 1 #The distance in metres travelled during the acceleration and decelleration time turning anticlockwise 

netA = Net("top blank", "bottom blank", "green", "pink", "orange", "yellow") #Initalises the default net A assuming that we are facing the corner 0 side
netB = Net("top blank", "bottom blank", "green", "orange", "pink", "yellow") #Initalises the default net B assuming that we are facing the corner 0 side
netC = Net("top blank", "bottom blank", "green", "yellow", "pink", "orange") #Initalises the default net C assuming that we are facing the corner 0 side

upOrientation = [-20, 20] #Determines the lower and upper bounds for what could be considered the face of the cube being oriented up
leftOrientation = [70, 110] #Determines the upper and lower for what could be considered the face of the cube being oriented left
downOrientation = [-160, -180, 160, 180] #Determines the upper and lower for what could be considered the face of the cube being oriented down. This is seperated into two sets of limits since it changes between positive and negative values.
rightOrientation = [-70, -110] #Determines the upper and lower for what could be considered the face of the cube being oriented right

#teamNo = R.zone() For competition use only
teamNo = 0 #A value between 0 and 3

if teamNo == 0:
    teamColour = "green"
elif teamNo == 1:
    teamColour = "orange"
elif teamNo == 2:
    teamColour = "pink"
else:
    teamColour = "yellow"


#---------------------------------------------------------------------------------------MOVEMENT FUNCTIONS-------------------------------------------------------------------------------------

def accelerate(start, end, turn=False, direction=None): #Turn defaults to false so that parameter is not required for a standard forward/backward call of the function. Additionaly direction is set to None so that it is also an optional argument as it is only necessary when calling the turn acceleration version of this function
    
    if turn: #Accelerating for a turn so need the wheels to power in opposite directions
        
        if direction == True: #True  means a clockwise turn
            
            if start < end: #Start is less than end so it is an acceleration
            
                while start < end: #Loops until start is greater than or equal to end
                    lwheel.power += accelerationStep
                    rwheel.power -= accelerationStep
                    start += accelerationStep
                    sleep(accelerationTime)
                
                lwheel.power = end #Sets both wheels exactly to the end value in case the difference between start and end was not a multiple of accelerationStep
                rwheel.power = end
                start = end
            
            else: #Start must therefore be less than end and so it is a decceleration process
                
                while start > end: #Loops until start is less than or equal to end
                    lwheel.power -= accelerationStep
                    rwheel.power += accelerationStep
                    start -= accelerationStep
                    sleep(accelerationTime)
                    
                lwheel.power = end #Sets both wheels exactly to the end value in case the difference between start and end was not a multiple of accelerationStep
                rwheel.power = end
                start = end
                
        elif direction == False: #False means an anticlockwise turn
            
            if start < end: #Start is less than end so it is an acceleration
            
                while start < end: #Loops until start is greater than or equal to end
                    lwheel.power -= accelerationStep
                    rwheel.power += accelerationStep
                    start += accelerationStep
                    sleep(accelerationTime)
                
                lwheel.power = end #Sets both wheels exactly to the end value in case the difference between start and end was not a multiple of accelerationStep
                rwheel.power = end
                start = end
            
            else: #Start must therefore be less than end and so it is a decceleration process
                
                while start > end: #Loops until start is less than or equal to end
                    lwheel.power += accelerationStep
                    rwheel.power -= accelerationStep
                    start -= accelerationStep
                    sleep(accelerationTime)
                    
                lwheel.power = end #Sets both wheels exactly to the end value in case the difference between start and end was not a multiple of accelerationStep
                rwheel.power = end
                start = end
        
        
    
    else: #Going forward/backward so both wheels need to be put to the same power
        
        if start < end: #Start is less than end so it is an acceleration
            
            while start < end: #Loops until start is greater than or equal to end
                lwheel.power += accelerationStep
                rwheel.power += accelerationStep
                start += accelerationStep
                sleep(accelerationTime)
            
            lwheel.power = end #Sets both wheels exactly to the end value in case the difference between start and end was not a multiple of accelerationStep
            rwheel.power = end
            start = end
        
        else: #Start must therefore be less than end and so it is a decceleration process
            
            while start > end: #Loops until start is less than or equal to end
                lwheel.power -= accelerationStep
                rwheel.power -= accelerationStep
                start -= accelerationStep
                sleep(accelerationTime)
                
            lwheel.power = end #Sets both wheels exactly to the end value in case the difference between start and end was not a multiple of accelerationStep
            rwheel.power = end
            start = end
            

def forward(distance): #Goes forward distance metres at 50 speed
    accelerate(0, 50)
    sleep((distance*forwardCallibration)-forwardAccelerationOffset)
    accelerate(50, 0)
    
def quickForward(distance): #Goes forward distance metres at full (100) speed
    accelerate(0, 100)
    sleep((distance*quickForwardCallibration)-quickForwardAccelerationOffset)
    accelerate(100, 0)
    
def reverse(distance): #Reverses distance metres at 50 speed
    accelerate(0, -50)
    sleep((distance*reverseCallibration)-reverseAccelerationOffset)
    accelerate(-50, 0)
    
def turnClockwise(angle): #Turns clockwise angle degrees at 50 speed
    accelerate(0, 50, True, True)
    sleep(((angle/90)*clockwiseCallibration)-clockwiseAccelerationOffset)
    accelerate(50, 0, True, True)

def turnAnticlockwise(angle): #Turns anticlockwise angle degrees at 50 speed
    accelerate(0, 50, True, False)
    sleep(((angle/90)*anticlockwiseCallibration)-anticlockwiseAccelerationOffset)
    accelerate(50, 0, True, False)
    
#---------------------------------------------------------------------------------------VISION FUNCTIONS-------------------------------------------------------------------------------------


def getClosestToken(xRes=800, yRes=800): #Gets the closest token that is in its field of view. Returns the token as an object, or an empty string if their are no tokens in its vision cone. The xRes and yRes params are optional and control the resolution of the picture that is taken
    markers = R.see(xRes,yRes)
    currDist = 100000000
    currMarker = ""
    
    for marker in markers:
        if marker.info.marker_type == MARKER_TOKEN_TOP or marker.info.marker_type == MARKER_TOKEN_SIDE or marker.info.marker_type == MARKER_TOKEN_BOTTOM and marker.dist < currDist:
            currMarker = marker
            currDist = marker.dist
    
    return currMarker